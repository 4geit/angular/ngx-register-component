import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxRegisterComponent } from './ngx-register.component';

describe('register', () => {
  let component: register;
  let fixture: ComponentFixture<register>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ register ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(register);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
