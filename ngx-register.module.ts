import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxRegisterComponent } from './ngx-register.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    NgxRegisterComponent
  ],
  exports: [
    NgxRegisterComponent
  ]
})
export class NgxRegisterModule { }
