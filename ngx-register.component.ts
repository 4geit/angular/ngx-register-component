import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-register',
  template: require('pug-loader!./ngx-register.component.pug')(),
  styleUrls: ['./ngx-register.component.scss']
})
export class NgxRegisterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
